# wsas

WebSocket Asset Streaming - stream binary assets over a WebSocket for quicker delivery. Assets are requested through an HTTP-like TOML schema. Once a request is received, the server searches a predefined "asset directory" for an asset matching the correct path. Once found, the server returns a request with the response TOML object and the binary data of the file, separated by a `----WSAS_BOUNDARY----` boundary.

WSAS uses the `fetch` method to get data, the `send` method to update data, and the `delete` method to delete data.

## Example Request

Request a file named `index.html`:

```toml
method = "fetch"
path = "index.html"
```

## Example Response

Response from previous request:

```toml
status = 200

[request]
method = "fetch"
path = "index.html"

[head]
type = "text/html"
size = 264----WSAS_BOUNDARY----<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
    </head>
    <body>
        <h1>Hello, world!</h1>
    </body>
</html>
```

## Example Request (send data)

A request which sends data to the server could look like:

```toml
method = "send"
path = "api/update"
body = "{\"key\":\"value\"}"
```

## Example Response (send data)

```toml
status = 200

[request]
method = "send"
path = "api/update"
body = "{\"key\":\"value\"}"

[head]
type = "text/plain"
size = 7----WSAS_BOUNDARY----Updated
```

## Example Response (404)

Below is an example response for a request to an asset which does not exist:

```toml
status = 400

[request]
method = "fetch"
path = "not-real"

[head]
type = "text/plain"
size = 14----WSAS_BOUNDARY----404: Not Found
```
