/**
 * @file Handle WSAS client
 * @name WSAS_Client
 * @license MIT
 */

import type { WSAS_Request, WSAS_Response } from ".";

import { stringify as TStringify, parse as TParse } from "smol-toml";
export const TOML = { stringify: TStringify, parse: TParse };

import mime from "mime";

/**
 * @export
 * @class WSAS_Client
 */
export default class WSAS_Client {
    public readonly socket: WebSocket;

    private WaitingForResponse: {
        // store elements by path
        [key: string]: (HTMLImageElement | HTMLVideoElement | HTMLIFrameElement)[];
    } = {};

    private RequestResponseQueue: {
        // store callbacks by path
        [key: string]: ((response: WSAS_Response) => void)[];
    } = {};

    private Blobs: { [key: string]: string } = {}; // store cached response urls

    /**
     * Creates an instance of WSAS_Client.
     * @param {string} url
     * @memberof WSAS_Client
     */
    constructor(url: string) {
        this.socket = new WebSocket(url);

        // listeners

        // ...open
        this.socket.addEventListener("open", () => {
            console.info("WSAS connection established.");
            this.ScanElements();
        });

        // ...message
        this.socket.addEventListener(
            "message",
            async (message: {
                data: { arrayBuffer: () => Promise<ArrayBuffer> };
            }) => {
                // get arraybuffer
                const buffer = await message.data.arrayBuffer();
                const array = new Uint8Array(buffer);

                // decode
                const decoded = new TextDecoder("utf8").decode(array);

                // get response
                const response = TOML.parse(
                    decoded.split("----WSAS_BOUNDARY----")[0]
                ) as any as WSAS_Response;

                // ...set body
                response.body = new Uint8Array(
                    array.buffer.slice(
                        // get all bytes AFTER request + boundary bytes
                        // these are just text, so their string lengths are the same as their byte length
                        TOML.stringify(response).length +
                            "----WSAS_BOUNDARY----".length
                    )
                );

                // fill waiting requests
                if (this.RequestResponseQueue[response.request.path!])
                    for (const callback of this.RequestResponseQueue[
                        response.request.path!
                    ])
                        callback(response);

                // fill waiting elements
                if (!this.WaitingForResponse[response.request.path!]) return; // nothing is expecting this file!

                for (const element of Array.from(
                    this.WaitingForResponse[response.request.path!]
                )) {
                    const UseSrc = element.hasAttribute("wsas-src") === true;

                    element.removeAttribute("wsas-src");
                    element.removeAttribute("wsas-href");

                    /* if (this.Blobs[response.request.path!]) {
                        element.src = this.Blobs[response.request.path!];
                        continue;
                    } */

                    // create blob
                    const url = URL.createObjectURL(
                        new Blob([response.body], {
                            type: response.head.type,
                        })
                    );

                    // store url
                    this.Blobs[response.request.path!] = url;

                    if (UseSrc) element.src = url;
                    else element.setAttribute("href", url);
                }
            }
        );

        // ...error
        this.socket.addEventListener("error", () => {
            console.error("Connection to WSAS socket errored!");
        });

        // ...close
        this.socket.addEventListener("close", () => {
            console.error(
                "Connection to WSAS socket closed! Assets will be unavailable."
            );
        });
    }

    /**
     * @method ScanElements
     * @description Scan for wsas-src attributes
     *
     * @return {void}
     * @memberof WSAS_Client
     */
    public ScanElements(): void {
        for (const element of (
            Array.from(document.querySelectorAll("[wsas-src]") as any) as any[]
        ).concat(Array.from(document.querySelectorAll("[wsas-href]")))) {
            const src =
                element.getAttribute("wsas-src") ||
                element.getAttribute("wsas-href");

            if (!src) continue;

            // fetch file
            if (!this.WaitingForResponse[src]) this.WaitingForResponse[src] = [];

            this.WaitingForResponse[src].push(element);

            // ...send request
            this.socket.send(
                TOML.stringify({
                    method: "fetch",
                    path: src,
                    headers: {
                        Wants: mime.getType(src),
                    },
                })
            );
        }
    }

    /**
     * @method fetch
     *
     * @param {WSAS_Request} request
     * @return {Promise<WSAS_Response>}
     * @memberof WSAS_Client
     */
    public fetch(request: WSAS_Request): Promise<WSAS_Response> {
        return new Promise(async (resolve, reject) => {
            // wait for socket to open
            if (this.socket.readyState !== this.socket.OPEN)
                await (() => {
                    return new Promise((resolve, reject) =>
                        this.socket.addEventListener("open", resolve)
                    );
                })();

            // send request
            if (!this.RequestResponseQueue[request.path])
                this.RequestResponseQueue[request.path] = [];

            this.RequestResponseQueue[request.path].push((res) => resolve(res));
            this.socket.send(TOML.stringify(request));
        });
    }
}
