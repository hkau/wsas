/**
 * @file WebSocket Asset Streaming
 * @name index.ts
 * @license MIT
 */

import { ServerWebSocket } from "bun";
import path from "node:path";

import { stringify as TStringify, parse as TParse } from "smol-toml";
export const TOML = { stringify: TStringify, parse: TParse };

import mime from "mime";

// types
export type WSAS_Config = {
    asset_directory: string; // supporting ":cwd" syntax for current working directory
    methods: WSAS_Method[]; // @ methods, ex: "@fetch FILE BASE64 /image.png", "@fetch FILE TEXT /index.html"
};

// ...request/response
export type WSAS_Headers = {
    Wants?: string; // what the request is expecting, ex: "image/png"
};

export type WSAS_Head = {
    type: string;
    size?: number; // byte length of file
};

export type WSAS_Request<T = Uint8Array> = {
    sock: ServerWebSocket<unknown>;
    method: WSAS_Method;
    path: string;
    headers?: WSAS_Headers;
    body?: T; // not available for "fetch" methods
};

export type WSAS_Response<T = Uint8Array | string> = {
    status: number; // HTTP status codes
    request: Partial<WSAS_Request>;
    head: WSAS_Head; // information about file
    body: T;
};

// ...routes
export type WSAS_Method = "fetch" | "send" | "delete";

export type WSAS_Route = {
    path: string;
    method: WSAS_Method;
    type?: "matches" | "begins";
    handler: (request: WSAS_Request) => Promise<WSAS_Response>; // must be async!
};

/**
 * @export
 * @class WSAssetStreamer
 */
export default class WSAssetStreamer {
    public readonly config: WSAS_Config;

    // ...store routes
    private routes: WSAS_Route[] = [];

    // ...decoders
    private readonly TextDecoder = new TextDecoder();
    private readonly TextEncoder = new TextEncoder();

    /**
     * Creates an instance of WSAssetStreamer.
     * @param {WSAS_Config} config
     * @memberof WSAssetStreamer
     */
    constructor(config: WSAS_Config) {
        this.config = config;
    }

    /**
     * @method register
     * @description Register route
     *
     * @param {WSAS_Route} config
     * @return {Promise<WSAS_Route>}
     * @memberof WSAssetStreamer
     */
    public async register(config: WSAS_Route): Promise<WSAS_Route> {
        this.routes.push(config);
        return config;
    }

    /**
     * @method JoinBuffer
     *
     * @private
     * @param {Uint8Array} first
     * @param {Uint8Array} second
     * @return {*}  {Uint8Array}
     * @memberof WSAssetStreamer
     */
    private JoinBuffer(first: Uint8Array, second: Uint8Array): Uint8Array {
        // data MUST be sent like this, otherwise it will be UTF-8 encoded and will rise in size A LOT
        // sending it like this keeps the binary content the original size, adding only the size of the first Uint8Array

        // define boundary
        const boundary = this.TextEncoder.encode("----WSAS_BOUNDARY----");

        // merge
        let Merged = new Uint8Array(first.length + boundary.length + second.length);

        Merged.set(first);
        Merged.set(boundary, first.length);
        Merged.set(second, boundary.length + first.length);

        // return
        return Merged;
    }

    /**
     * @method resolve
     *
     * @param {ServerWebSocket<unknown>} sock
     * @param {(string | ArrayBuffer | Uint8Array)} message
     * @return {Promise<Uint8Array>}
     * @memberof WSAssetStreamer
     */
    public async resolve(
        sock: ServerWebSocket<unknown>,
        message: string | ArrayBuffer | Uint8Array
    ): Promise<Uint8Array> {
        // decode message
        if (typeof message !== "string") message = this.TextDecoder.decode(message);
        sock.subscribe("wsas-global-deliver");

        // parse message
        const parsed: Partial<WSAS_Request> = TOML.parse(message);

        // build full request
        if (
            !parsed.method ||
            !parsed.path ||
            (parsed.method !== "fetch" && !parsed.body)
        )
            return this.JoinBuffer(
                this.TextEncoder.encode(
                    TOML.stringify({
                        status: 400,
                        request: parsed,
                        head: {
                            type: "text/plain",
                            size: 16,
                        },
                    })
                ),
                this.TextEncoder.encode("400: Bad Request")
            );

        if (parsed.path.startsWith("/")) parsed.path = parsed.path.slice(1); // remove / from path
        parsed.sock = undefined;

        const request: WSAS_Request = {
            sock,
            method: parsed.method || "fetch",
            path: parsed.path || "/",
            body: parsed.body,
            headers: parsed.headers,
        };

        // search for route
        const route = this.routes.find(
            (r) =>
                (r.path === request.path && r.method === request.method) ||
                (r.type === "begins" &&
                    request.path.startsWith(r.path) &&
                    r.method === request.method)
        );

        if (!route) {
            // ...check if we're referencing a file
            const file = Bun.file(
                path.resolve(
                    (this.config.asset_directory || import.meta.dir).replace(
                        ":cwd",
                        process.cwd()
                    ),
                    request.path
                )
            );

            if (await file.exists()) {
                // ...serve file!
                const buffer = await file.arrayBuffer();

                return this.JoinBuffer(
                    this.TextEncoder.encode(
                        TOML.stringify({
                            status: 200,
                            request: parsed,
                            head: {
                                type:
                                    mime.getType(parsed.path) ||
                                    "application/octet-stream",
                                size: buffer.byteLength,
                            },
                        })
                    ),
                    new Uint8Array(buffer)
                );
            }

            // ...return 404
            return this.JoinBuffer(
                this.TextEncoder.encode(
                    TOML.stringify({
                        status: 400,
                        request: parsed,
                        head: {
                            type: "text/plain",
                            size: 14,
                        },
                    })
                ),
                this.TextEncoder.encode("404: Not Found")
            );
        }

        // run route
        const res = await route.handler(request);
        let body = (await route.handler(request)).body;
        if (typeof body === "string") body = this.TextEncoder.encode(body);

        // ...remove extra values that the client doesn't need
        res.request.sock = undefined;

        res.body = "";
        res.request.body = undefined;

        // ...fill head values
        res.head.size = body.length;

        // ...return
        return this.JoinBuffer(this.TextEncoder.encode(TOML.stringify(res)), body);
    }
}
