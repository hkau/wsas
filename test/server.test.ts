import WSAssetStreamer, { WSAS_Request } from "../src";

import path from "node:path";
import mime from "mime";

// create asset streamer
const AssetStreamer = new WSAssetStreamer({
    asset_directory: import.meta.dir,
    methods: ["fetch"],
});

AssetStreamer.register({
    method: "fetch",
    path: "test",
    type: "begins",
    handler: async (request) => {
        return {
            status: 200,
            request,
            head: {
                type: "text/plain",
            },
            body: "This is a custom route",
        };
    },
});

// serve
Bun.serve<WSAS_Request>({
    port: 8080,
    async fetch(request, server) {
        const url = new URL(request.url);

        if (url.pathname === "/api/wsas") {
            const success = server.upgrade(request);

            return success
                ? undefined
                : new Response("WebSocket upgrade error", { status: 400 });
        }

        // ...
        const _path = url.pathname === "/" ? "index.html" : url.pathname;
        const file = Bun.file(path.join(import.meta.dir, _path));

        // make sure file exists
        if (!(await file.exists()))
            return new Response("404: Not Found", { status: 404 });

        // return file
        return new Response(await file.text(), {
            headers: {
                "Content-Type": mime.getType(_path) || "text/plain",
            },
        });
    },
    websocket: {
        message: async (sock, message) => {
            // resolve
            const response = await AssetStreamer.resolve(sock, message);

            // send response
            sock.sendBinary(Buffer.from(response), true);
        },
    },
});
