import { build } from "bun";
import fs from "node:fs";

if (process.env.DO_NOT_CLEAR_DIST === undefined)
    fs.rmSync("./dist", { recursive: true }); // reset dist directory

const output = await build({
    entrypoints: ["./src/index.ts", "./src/WSAS_Client.ts"],
    minify: {
        identifiers: true,
        syntax: true,
        whitespace: true,
    },
    target: "bun",
    outdir: "dist",
    splitting: true,
    naming: {
        asset: "[name].[ext]",
        chunk: "[name]-[hash].[ext]",
        entry: "[name].[ext]", // do not include [dir]!!! files will NOT be findable if you include [dir]!!!
    },
    external: [
        // these files won't be included in the build
        "package.json",
    ],
});

// log finished
console.log("\x1b[30;100m info \x1b[0m Build finished!");
